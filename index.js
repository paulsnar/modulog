'use strict';

const  chalk = require('chalk'),
      moment = require('moment')


const LEVELS = ['log', 'debug', 'info', 'warning', 'error']

function generic_format(type, name, date, ...others) {
  let fmt_name = `[${name}]`;
  if (type === 'error') {
    fmt_name = chalk.bold.red('X ') + chalk.red(fmt_name)
  } else if (type === 'warning') {
    fmt_name = chalk.bold.yellow('! ') + chalk.yellow(fmt_name)
  } else if (type === 'info') {
    fmt_name = chalk.bold.cyan('i ') + chalk.cyan(fmt_name)
  } else if (type === 'debug') {
    fmt_name = chalk.bold.gray('@ ') + chalk.gray(fmt_name)
  } else if (type === 'log') {
    fmt_name = chalk.bold.green('~ ') + chalk.green(fmt_name)
  } else {
    fmt_name = chalk.bold('# ') + fmt_name
  }

  let fmt_date = chalk.gray(date.toISOString())

  return [fmt_name, fmt_date, ...others].join(' ')
}

function log(level, name, format, ...data) {
  let date = moment()
  let fmt = generic_format(level, name, date, format)
  console.log(fmt, ...data)
}

LEVELS.forEach((level) => {
  exports[level] = log.bind(null, level)
});

exports.generic = log.bind(null, null)

exports.bound = function(boundName) {
  let bound = { }
  LEVELS.forEach((level) => {
    bound[level] = exports[level].bind(null, boundName)
  })
  bound.generic = exports.generic.bind(null, boundName)
  return bound
}
