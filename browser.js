(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(factory)
  } else if (typeof exports === 'object') {
    module.exports = factory()
  } else {
    root.L = factory()
  }
})(this, function() {
  'use strict';

  var LEVELS = ['log', 'debug', 'info', 'warning', 'error']

  var _format = function(type, name, date) {
    var rest = Array.prototype.slice.call(arguments, 3)

    var fmt = ['%c%s %c[%s] %c%s %c' + rest[0],
      'font-weight: bold', '#',
      '', name,
      'color: gray', date.toISOString(),
      ''].concat(rest.slice(1))
    if (type === 'error') {
      fmt[1] = 'color: red; font-weight: bold'
      fmt[2] = 'X'
      fmt[3] = 'color: #fd0606'
    } else if (type === 'warning') {
      fmt[1] = 'color: #ffdf28; font-weight: bold'
      fmt[2] = '!'
      fmt[3] = 'color: #e8cc2d'
    } else if (type === 'info') {
      fmt[1] = 'color: #08e2fd; font-weight: bold'
      fmt[2] = 'i'
      fmt[3] = 'color: #08c1fd'
    } else if (type === 'debug') {
      fmt[1] = 'color: gray; font-weight: bold'
      fmt[2] = '@'
      fmt[3] = 'color: gray'
    } else if (type === 'log') {
      fmt[1] = 'color: #33de00; font-weight: bold'
      fmt[2] = '~'
      fmt[3] = 'color: #41dc13'
    }

    return fmt
  }
  var _log = function(level, name) {
    var rest = Array.prototype.slice.call(arguments, 2)

    var date = new Date()
    var fmt = _format.apply(L, [level, name, date].concat(rest))
    console.log.apply(console, fmt)
  }
  var L = { }
  LEVELS.forEach(function(level) {
    L[level] = _log.bind(L, level)
  })
  L.generic = _log.bind(L, null)

  L.bound = function(boundName) {
    var bound = { }
    LEVELS.forEach(function(level) {
      bound[level] = L[level].bind(L, boundName)
    })
    bound.generic = L.generic.bind(L, boundName)
    return bound
  }

  L.LEVELS = LEVELS

  return L
})
