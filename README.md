# Modulog

Modular, pretty logging both for the console and the browser.

## Usage

In Node (console-based environments):

```js
const modulog = require('modulog')

modulog.info('something', 'This is a format string! %s', '(string)')
// -> "i [something] 2016-10-23T18:49:17 This is a format string! (string)"

const bound = L.bound('some module')
bound.warning('This is a warning from some module')
// -> "! [some module] 2016-10-23T18:49:17 This is a warning from some module"
```

Node version peer-depends on Chalk and Moment.

In the browser:

```js
L.info('something', 'This is a format string! %s', '(string)')
// -> "i [something] 2016-10-23T18:49:17 This is a format string! (string)"

var bound = L.bound('some module')
bound.warning('This is a warning from some module')
// -> "! [some module] 2016-10-23T18:49:17 This is a warning from some module"
```

You can get L by either AMD, CommonJS bundler or just using it globally.

(Note that the browser version currently can only bind to the global L, and
works pretty much everywhere except for [JSConsole][], which doesn't work with
any kind of format strings.)

[JSConsole]: https://jsconsole.com
